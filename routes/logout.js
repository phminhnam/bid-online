var express = require("express");
var router = express.Router();

router.get("/", function(req, res) {
  if (req.user != null) {
    req.logout();
    res.render("login");
  } else res.send("Chưa đăng nhập");
});
module.exports = router;
