var express = require("express");
var bodyParse = require("body-parser");
var router = express.Router();
var comment = require("../models/CommentModel");

var jsonParser = bodyParse.json();

router.get("/", function (req, res) {
    comment.getAll().then(data => {
        res.send(data);
    }).catch(e => res.sendStatus(500))
})

router.get("/:id", function (req, res) {
    comment.GetById(req.params.id).then(data => res.send(data)).catch(e => res.sendStatus(400))
})

router.post("/", jsonParser, function (req, res) {
    comment.Create(req.body.content, req.body.like, req.body.account_Id, req.body.bid_Id).then(data => res.send(data)).catch(e => res.sendStatus(400))
})

router.put("/:id", jsonParser, function (req, res) {
    comment.Update(req.params.id, req.body.content, req.body.like).then(data => res.send(data)).catch(e => res.sendStatus(400))
})

router.delete("/:id", function (req, res) {
    comment.delete().then(status => res.sendStatus(status)).catch(e => sendStatus(400))
})

module.exports = router;