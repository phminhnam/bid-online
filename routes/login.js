var express = require("express");
const Passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
var router = express.Router();
var login = require("../models/accountModel");

router.get("/", (req, res) => res.render("login"));

router.post(
  "/",
  Passport.authenticate("local", {
    failureRedirect: "/login",
    successRedirect: "/profile"
  })
);

Passport.use(
  new LocalStrategy(async (username, password, done) => {
    var result = await login.checkLogin(username, password);
    if (result == false) return done(null, false);
    else return done(null, result);
  })
);
Passport.serializeUser((user, done) => {
  done(null, user);
});

Passport.deserializeUser(async (user, done) => {
  var result = await login.findUser(user).then(data => {
    if (data == user) return data;
    else return false;
  });
  return done(null, result);
});

module.exports = router;
