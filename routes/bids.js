var express = require("express");
var bodyParse = require("body-parser");
var router = express.Router();
var bid = require("../models/BidModel");

var jsonParser = bodyParse.json();

router.get("/", function(req, res) {
  bid
    .getAll()
    .then(data => res.send(data))
    .catch(e => res.sendStatus(500));
});

router.get("/:id", async function(req, res) {
  var count, price;
  console.log(req.params.id);
  await bid_history
    .CountByBidId(req.params.id)
    .then(result => (count = result));
  bid
    .GetById(req.params.id)
    .then(data => {
      res.render("bid_detail", { data: data, count: count, isLogin: req.user });
    })
    .catch(e => res.sendStatus(500));
});

router.post("/", function(req, res) {
  bid
    .Create(
      req.body.start_price,
      req.body.end_price,
      req.body.name_product,
      req.body.content,
      req.body.account_id,
      req.body.category_id
    )
    .then(data => res.send(data))
    .catch(e => res.sendStatus(400));
});

router.put("/:id", jsonParser, function(req, res) {
  bid
    .Update(req.params.id, req.body.bid_price, req.user)
    .then(data => res.send(data))
    .catch(e => res.sendStatus(400));
});

router.delete("/:id", function(req, res) {
  bid
    .Delete(req.params.id)
    .then(data => res.send(data))
    .catch(e => res.sendStatus(400));
});
module.exports = router;
