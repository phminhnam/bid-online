var express = require("express");
var router = express.Router();
var register = require("../models/accountModel");
var bodyParse = require("body-parser");

var jsonParser = bodyParse.json();

router.get("/", (req, res) => res.render("register", { error: "" }));

router.post("/", jsonParser, async function(req, res) {
  var user = await register
    .findUser(req.body.email)
    .catch(e => res.sendStatus(400));
  if (user == false) {
    register.addUser(
      req.body.email,
      req.body.password,
      req.body.birthday,
      req.body.fullname
    );
    res.render("login");
  } else res.render("register", { error: "Email is existed!" });
});

module.exports = router;
