var express = require("express");
var editProfile = require("../models/accountModel");
var bodyParse = require("body-parser");
var router = express.Router();

var jsonParser = bodyParse.json();
router.get("/", async (req, res) => {
  if (req.isAuthenticated()) {
    var acc = await editProfile.GetAccount(req.user);
    res.render("editProfile", { data: acc });
  } else res.send("Chua dang nhap");
});

router.post("/", jsonParser, async function(req, res) {
  var user = await editProfile
    .EditProfile(req.user, req.body.birthday, req.body.fullname)
    .catch(e => res.sendStatus(400));
  res.redirect("/profile");
});

module.exports = router;
