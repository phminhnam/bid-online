var express = require("express");
var changePass = require("../models/accountModel");
var bodyParse = require("body-parser");

var router = express.Router();

var jsonParser = bodyParse.json();
router.get("/", async (req, res) => {
  if (req.isAuthenticated()) {
    var acc = await changePass.GetAccount(req.user);
    res.render("changepass", { data: acc, error: "" });
  } else res.send("Chua dang nhap");
});

router.post("/", jsonParser, async function(req, res) {
  if (req.body.newPassword == req.body.rePassword) {
    var check = await changePass.checkLogin(req.user, req.body.oldPassword);
    if (check != false) {
      await changePass
        .changePassword(req.user, req.body.newPassword)
        .then(data => res.redirect("/profile"))
        .catch(e => res.sendStatus(400));
    } else {
      var acc = await changePass.GetAccount(req.user);
      res.render("changepass", { data: acc, error: "Old Password is invalid" });
    }
  } else {
    var acc = await changePass.GetAccount(req.user);
    res.render("changepass", { data: acc, error: "New rePassword is invalid" });
  }
});
module.exports = router;
