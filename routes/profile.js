var express = require("express");
var profile = require("../models/accountModel");
var category = require("../models/CategoryModel");
var bodyParse = require("body-parser");
var router = express.Router();
var crypto = require("crypto-js");

var jsonParser = bodyParse.json();

router.get("/", async (req, res) => {
  if (req.isAuthenticated()) {
    var acc = await profile.GetAccount(req.user);
    var check = await profile.CheckSeller(req.user);
    if (acc.role == 0) {
      var emails = await profile.GetUserUpgrade();
      var lsAcc = await profile.GetAllAccount();
      var lsCate = await category.getAll();
      res.render("profile", {
        data: acc,
        check: check,
        emails: emails,
        isLogin: req.user,
        lsAcc: lsAcc,
        lsCate: lsCate
      });
    } else if (acc.role == 1) {
      var bids = await profile.getBid(req.user);
      res.render("profile", {
        data: acc,
        check: check,
        isLogin: req.user,
        bids: bids
      });
    } else if (acc.role == 2) {
      res.render("profile", {
        data: acc,
        check: check,
        isLogin: req.user
      });
    }
  } else res.render("login");
});

router.post("/", jsonParser, async function(req, res) {
  await profile.ToSeller(req.user).catch(e => res.sendStatus(400));
  res.redirect("/profile");
});
router.delete("/grantRole/:user_id", async function(req, res) {
  await profile.UpgradeUser(req.params.user_id);
  res.sendStatus(200);
});
router.put("/reduction/:email", jsonParser, async function(req, res) {
  await profile.UpdateRole(req.body.email);
  res.sendStatus(200);
});
module.exports = router;
