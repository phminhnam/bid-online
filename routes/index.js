var express = require("express");
var router = express.Router();
var category = require("../models/CategoryModel");
var bid = require("../models/BidModel");

/* GET home page. */
router.get("/page=:page", async function(req, res, next) {
  var categories;
  var bids;
  await category
    .getAll(req)
    .then(result => (categories = result))
    .catch(e => res.sendStatus(500));
  await bid
    .getAll(req.params.page)
    .then(result => (bids = result))
    .catch(e => res.sendStatus(500));
  console.log(bids);
  console.log(bids.length);
  res.render("index", {
    categories: categories,
    bids: bids,
    page: req.params.page
  });
  res.end();
});

router.get("/", function(req, res) {
  res.redirect("/page=1");
});

router.get("/category=:category_id", async function(req, res) {
  var categories;
  var bids;
  await category
    .getAll(req)
    .then(result => (categories = result))
    .catch(e => res.sendStatus(500));
  await bid
    .GetByCategory(req.params.category_id)
    .then(result => (bids = result))
    .catch(e => res.sendStatus(500));

  res.render("index", {
    categories: categories,
    bids: bids,
    page: req.params.page
  });
  res.end();
});
module.exports = router;
