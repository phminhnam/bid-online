var express = require("express");
var category = require("../models/CategoryModel");
var bodyParse = require("body-parser");
var router = express.Router();

var jsonParser = bodyParse.json();
/* GET users listing. */
router.get("/", function(req, res) {
  category
    .getAll()
    .then(data => res.send(data))
    .catch(e => res.sendStatus(500));
});

router.get("/:id", jsonParser, function(req, res) {
  category
    .GetById(req.params["id"])
    .then(data => res.send(data))
    .catch(e => res.sendStatus(400));
});

router.post("/", jsonParser, async function(req, res) {
  var Id = await category.Create(req.body.name).catch(e => res.sendStatus(400));
});

router.put("/:id", jsonParser, async function(req, res) {
  category
    .Update(req.params.id, req.body.name)
    .then(data => res.send(data))
    .catch(e => res.sendStatus(400));
});

router.delete("/:id", function(req, res) {
  return res.sendStatus(category.Delete(req.params.id));
});

module.exports = router;
