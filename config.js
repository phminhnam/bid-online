var config = {
  debug: true,
  port: 3306,
  mysql: {
    host: "127.0.0.1",
    username: "root",
    password: "123456",
    database: "bid_online"
  }
};

module.exports = config;
