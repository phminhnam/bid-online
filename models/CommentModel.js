const sequelize = require("./common");
const helper = require("./helper");

exports.getAll = async () => {
  var statement = "SELECT * FROM comment";
  var result = await sequelize
    .query(statement)
    .then(data => helper.ParseDataToJson(data));
  console.log(result);
  return result;
};

exports.GetById = async Id => {
  var sql = `SELECT * FROM comment  WHERE comment.id = ${Id}`;
  var result = await sequelize
    .query(sql)
    .then(data => helper.ParseDataToJson(data));
  console.log(result);
  return result;
};

exports.Delete = Id => {
  var sql = `DELETE FROM comment WHERE comment.id = ${Id}`;
  sequelize.query(sql);
  return 200;
};

exports.Create = async (content, like, account_Id, bid_Id) => {
  var id = await helper.GenerateId("comment");

  const sql = `INSERT INTO comment(id, name, like, accountId, bidId) VALUES (${id}, N'${content}', ${like},${account_Id}, ${bid_Id})`;
  await sequelize.query(sql);
  var item = await sequelize.query(
    `SELECT * FROM comment WHERE comment.id =${id}`
  );
  return helper.ParseDataToJson(item);
};

exports.Update = async (id, content, like) => {
  const sql = `UPDATE comment SET content = '${content}', like = ${like} WHERE id = ${id}`;
  await sequelize.query(sql).catch(e => console.log(e));
  var item = await sequelize.query(
    `SELECT * FROM comment WHERE comment.id =${id}`
  );
  return helper.ParseDataToJson(item);
};
