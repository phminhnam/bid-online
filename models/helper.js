var sequelize = require("./common");
exports.GenerateId = async tableName => {
  var sql = `SELECT * FROM ${tableName} ORDER BY id DESC LIMIT 1`;
  let id = 1;
  await sequelize
    .query(sql)
    .then(Id => {
      if (Id[0].length == 0) return 1;
      const current = this.ParseDataToJson(Id);
      id = current[0].id + 1;
    })
    .catch(e => console.log(e));
  console.log(id);

  return id;

  //   if (currentId[0].length === 0) return 1;
  //   else return currentId[0].id;
};

exports.ParseDataToJson = Data => {
  const dataLength = Data[0].length;
  if (dataLength == 0) throw new Error("Can't parse data");
  // else if (dataLength == 1) {
  //   var str = JSON.stringify(Data[0]);
  //   var length = str.length - 1;
  //   // console.log(JSON.parse(str.substring(1, length)));
  //   return JSON.parse(str.substring(1, length));
  // }
  else {
    var result = [];
    Data[0].map(item => {
      var str = JSON.stringify(item);
      result.push(JSON.parse(str));
    });
    return result;
  }
};

exports.FilterById = (Id, table) => {
  var sql = `SELECT * FROM ${table} WHERE Id = ${Id}`;
  var result = sequelize.query(sql).then(data => this.ParseDataToJson(data));
  return result;
};

exports.GenerateIdFromEmail = async email => {
  var sql = `SELECT id FROM account WHERE account.email = '${email}'`;
  var result = await sequelize.query(sql).then(data => {
    if (data[0].length == 0) return false;
    else {
      var result1 = this.ParseDataToJson(data);
      return result1[0].id;
    }
  });
  return result;
};
exports.GenerateEmailFromId = async id => {
  var sql = `SELECT email FROM account WHERE account.id = '${id}'`;
  var result = await sequelize.query(sql).then(data => {
    if (data[0].length == 0) return false;
    else {
      var result1 = this.ParseDataToJson(data);
      return result1[0].email;
    }
  });
  return result;
};
