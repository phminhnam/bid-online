const sequelize = require("./common");
const helper = require("./helper");

exports.getAll = async () => {
  var statement = "SELECT * FROM category";
  var result = await sequelize
    .query(statement)
    .then(data => helper.ParseDataToJson(data));
  return result;
};

exports.GetById = async Id => {
  var sql = `SELECT id, name FROM category  WHERE category.id = ${Id}`;
  var result = await sequelize
    .query(sql)
    .then(data => helper.ParseDataToJson(data));
  console.log(result);
  return result;
};

exports.Delete = async Id => {
  var sql = `DELETE FROM category WHERE category.id = ${Id}`;
  await sequelize.query(sql);
  return 200;
};

exports.Create = async name => {
  var id = await helper.GenerateId("category");

  const sql = `INSERT INTO category(id, name) VALUES (${id}, N'${name}')`;
  await sequelize.query(sql);
  var item = await sequelize.query(
    `SELECT * FROM category WHERE category.id =${id}`
  );
  return helper.ParseDataToJson(item);
};

exports.Update = async (id, name) => {
  const sql = `UPDATE category SET name = '${name}' WHERE id = ${id}`;
  await sequelize.query(sql).catch(e => console.log(e));
  var item = await sequelize.query(
    `SELECT * FROM category WHERE category.id =${id}`
  );
  return helper.ParseDataToJson(item);
};
