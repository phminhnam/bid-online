const sequelize = require("./common");
const helper = require("./helper");

exports.getAll = async page => {
  var position = (page - 1) * 8;
  var statement = `SELECT * FROM bid WHERE bid.end_time > CURRENT_TIMESTAMP LIMIT ${position}, 8 `;
  var result = await sequelize
    .query(statement)
    .then(data => helper.ParseDataToJson(data));
  console.log(result);
  return result;
};

exports.GetById = async Id => {
  var sql = `SELECT * FROM bid  WHERE bid.id = ${Id}`;
  var result = await sequelize
    .query(sql)
    .then(data => helper.ParseDataToJson(data));
  console.log(result);
  return result;
};

exports.Delete = Id => {
  var sql = `DELETE FROM bid WHERE bid.id = ${Id}`;
  sequelize.query(sql);
  return 200;
};

exports.Create = async (
  start_price,
  end_price,
  name_product,
  content,
  account_id,
  category_id
) => {
  var id = await helper.GenerateId("bid");
  var start_time = new Date();
  var end_time = new Date(new Date().getDate());
  end_time.setDate(end_day.getDate() + 7);
  const sql = `INSERT INTO comment(id, start_time, end_time, start_price, end_price, now_price, name_product, content, account_id, category_id) 
            VALUES (${id}, N'${start_time.getDate()}', ${end_time}, ${start_price}, ${end_price}, 0, ${name_product}, ${content}, ${account_id}, ${category_id})`;
  await sequelize.query(sql);
  var item = await sequelize.query(`SELECT * FROM bid WHERE bid.id =${id}`);
  return helper.ParseDataToJson(item);
};

exports.Update = async (bid_id, now_price, email) => {
  var user_id = await helper.GenerateIdFromEmail(email);
  var id = await helper.GenerateId("bid_history");
  const sql = `UPDATE bid SET now_price = ${now_price} WHERE id = ${bid_id}`;
  await sequelize.query(sql).catch(e => console.log(e));
  await sequelize.query(
    `INSERT INTO bid_history(id, account_id, bid_id, price, time) VALUE ('${id}','${user_id}','${bid_id}','${now_price}',CURRENT_TIMESTAMP())`
  );
};
exports.GetByCategory = async id => {
  var sql = `SELECT * FROM bid, category  WHERE bid.category_id = category.id AND category.id = '${id}'`;
  var result = await sequelize
    .query(sql)
    .then(data => helper.ParseDataToJson(data));
  return result;
};
