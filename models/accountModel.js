const sequelize = require("./common");
const helper = require("../models/helper");
const moment = require("moment");
var crypto = require("crypto-js");

var crypto = require("crypto-js");

exports.checkLogin = async (email, password) => {
  var sql = `SELECT * FROM account WHERE account.email = '${email}'`;

  var result = await sequelize.query(sql).then(data => {
    if (data[0].length == 0) return false;
    const length = JSON.stringify(data[0]).length - 1;
    const str = JSON.stringify(data[0]);
    const dataUser = JSON.parse(str.substring(1, length));
    var bytes = crypto.AES.decrypt(dataUser.password, "hashPass");
    var passHash = bytes.toString(crypto.enc.Utf8);
    if (email == dataUser.email && password == passHash) {
      return dataUser.email;
    } else {
      return false;
    }
  });
  return result;
};
exports.addUser = async (email, password, birthday, fullname) => {
  var role = 2;
  var id = await helper.GenerateId("account");
  var passHash = crypto.AES.encrypt(password, "hashPass").toString();
  var sql = `INSERT INTO account(id, email, password, birthday, fullname, role) 
  VALUE('${id}','${email}','${passHash}','${birthday}','${fullname}','${role}')`;
  sequelize.query(sql);
};
exports.findUser = async email => {
  var sql = `SELECT * FROM account WHERE account.email = '${email}'`;
  var result = await sequelize.query(sql).then(data => {
    if (data[0].length == 0) return false;
    const length = JSON.stringify(data[0]).length - 1;
    const str = JSON.stringify(data[0]);
    const dataUser = JSON.parse(str.substring(1, length));
    if (email == dataUser.email) {
      return dataUser.email;
    } else {
      return false;
    }
  });
  return result;
};

exports.changePassword = async (email, password) => {
  var passHash = crypto.AES.encrypt(password, "hashPass").toString();
  var sql = `UPDATE account SET password = '${passHash}' WHERE email = '${email}'`;
  await sequelize.query(sql).catch(e => console.log(e));
  var item = await sequelize.query(
    `SELECT * FROM account WHERE account.email ='${email}'`
  );
  return item.email;
};

exports.GetAccount = async email => {
  var sql = `SELECT * FROM account WHERE account.email = '${email}'`;
  var result = await sequelize.query(sql).then(data => {
    if (data[0].length == 0) return false;
    else {
      const length = JSON.stringify(data[0]).length - 1;
      const str = JSON.stringify(data[0]);
      const dataUser = JSON.parse(str.substring(1, length));
      return dataUser;
    }
  });
  return result;
};

exports.EditProfile = async (email, birthday, fullname) => {
  var sql = `UPDATE account SET birthday = '${birthday}' , fullname= '${fullname}' WHERE email = '${email}'`;
  await sequelize.query(sql);
};

exports.ToSeller = async email => {
  var sql = `SELECT id FROM account WHERE account.email = '${email}' AND account.role = 2`;
  await sequelize.query(sql).then(async data => {
    if (data[0].length == 0) return false;
    else {
      var result = await helper.ParseDataToJson(data);
      var id = await helper.GenerateId("grant_role");
      var sql1 = `INSERT INTO grant_role(id, user_id) VALUE('${id}','${result[0].id}')`;
      sequelize.query(sql1);
    }
  });
};
exports.CheckSeller = async email => {
  var id = await helper.GenerateIdFromEmail(email);
  var sql = `SELECT id FROM grant_role WHERE grant_role.user_id = '${id}'`;
  var result = await sequelize.query(sql).then(data => {
    if (data[0].length == 0) return false;
    else return true;
  });
  return result;
};
exports.GetUserUpgrade = async () => {
  var sql = `SELECT account.email as email, account.id as id FROM grant_role, account WHERE grant_role.user_id = account.id`;
  var result = await sequelize.query(sql).then(async data => {
    if (data[0].length == 0) return [];
    else {
      var result1 = await helper.ParseDataToJson(data);
      return result1;
    }
  });
  return result;
};
exports.UpgradeUser = async user_id => {
  var sql1 = `UPDATE account SET role = 1 WHERE id = '${user_id}'`;
  await sequelize.query(sql1);
  var sql = `DELETE FROM grant_role WHERE grant_role.user_id = '${user_id}'`;
  await sequelize.query(sql);
  return 200;
};

exports.GetAllAccount = async () => {
  var sql = `SELECT * FROM account`;
  var result = await sequelize.query(sql).then(async data => {
    if (data[0].length == 0) return false;
    else {
      var result1 = await helper.ParseDataToJson(data);
      return result1;
    }
  });
  return result;
};
exports.UpdateRole = async email => {
  var sql = `UPDATE account SET role = 2 WHERE email = '${email}'`;
  await sequelize.query(sql).catch(e => console.log(e));
  return 200;
};
exports.getBid = async email => {
  var sql = `SELECT * FROM account,bid WHERE bid.account_id = account.id AND account.email = '${email}'`;
  var result = await sequelize.query(sql).then(async data => {
    if (data[0].length == 0) return [];
    else {
      var result1 = await helper.ParseDataToJson(data);
      result1.forEach(element => {
        element.start_time = moment(element.start_time).format(
          "DD/MM/YYYY h:mm a"
        );
        element.end_time = moment(element.end).format("DD/MM/YYYY h:mm a");
      });
      return result1;
    }
  });
  return result;
};
