-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: localhost    Database: bid_online
-- ------------------------------------------------------
-- Server version	8.0.16

<<<<<<< Updated upstream
=======

>>>>>>> Stashed changes
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `account` (
  `id` int(11) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `birthday` varchar(45) NOT NULL,
  `fullname` varchar(45) NOT NULL,
  `role` int(11) NOT NULL,
  `session` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES (1,'khacphuongit@gmail.com','U2FsdGVkX19YXDPnQO6o1kHV+prPewntHqrLvLlJOAA=','1997-12-03','Phùng Khắc Phương',2,NULL),(2,'nguyenvanan125@gmail.com','U2FsdGVkX18tAjAmuQJb6azGt/gThCWn1lT471fwqVs=','1996-05-12','Nguyễn Văn Ân',2,NULL),(3,'phamthihuong411@gmail.com','U2FsdGVkX18vQDIUryc51n5dMDEmwQgXpCpHu0uZvNs=','1990-11-04','Phạm Thị Hương',2,NULL),(4,'hoangvanbinh195@gmail.com','U2FsdGVkX1+uULZeoeYa3N8QRv60athLAO4hFb8/WS8=','1998-05-19','Hoàng Văn Bình',2,NULL),(5,'nguyenminhnhat@gmail.com','U2FsdGVkX18+Gn7YxTt/X37Pz0saiQFdmXBk5KuYq1g=','1993-12-12','Nguyễn Minh Nhật',2,NULL);
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bid`
--

DROP TABLE IF EXISTS `bid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `bid` (
  `id` int(11) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `comment` int(45) DEFAULT NULL,
  `start_price` int(11) NOT NULL,
  `end_price` int(11) NOT NULL,
  `now_price` int(11) NOT NULL,
  `name_product` varchar(45) NOT NULL,
  `content` varchar(500) NOT NULL,
  `account_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_BID_ACCPUNT_idx` (`account_id`),
  KEY `FK_BID_CATEGORY_idx` (`category_id`),
  CONSTRAINT `FK_BID_ACCOUNT` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_BID_CATEGORY` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bid`
--

LOCK TABLES `bid` WRITE;
/*!40000 ALTER TABLE `bid` DISABLE KEYS */;
INSERT INTO `bid` VALUES (1,'2020-01-01 12:00:00','2020-01-02 00:00:00',NULL,100000,0,0,'Sách Nhà Giả Kim','Tiểu thuyết Nhà giả kim của Paulo Coelho như một câu chuyện cổ tích giản dị, nhân ái, giàu chất thơ, thấm đẫm những minh triết huyền bí của phương Đông.',1,1),(2,'2020-01-01 12:00:00','2020-01-02 00:00:00',NULL,200000,0,0,'Sách Quốc Gia Khởi Nghiệp','Con người có xu hướng ỷ lại vào trí nhớ hơn là óc tưởng tượng. Ký ức đồng nghĩa với những điều quen thuộc, còn trí tưởng tượng chỉ mang đến những điều xa lạ. Vì thế, trí tưởng tượng đôi khi đáng sợ, nó đòi hỏi người ta phải mạo hiểm từ bỏ những gì thân quen.',1,1),(3,'2020-01-02 12:00:00','2020-01-03 00:00:00',NULL,100000,0,0,'Sách 999 Lá Thư Gửi Cho Chính Mình','Nơi để bạn có thể sống chậm lại, ngẫm nghĩ lại chính bản thân mình của quá khứ và hiện tại. Tác giả đã rất khéo léo trong việc truyền tải những thông điệp về cuộc sống, tình yêu, tương lai… đến những người trẻ trong xã hội hiện nay.',2,1),(4,'2020-01-02 12:00:00','2020-01-03 00:00:00',NULL,100000,0,0,'Sách Cảm Ơn Người Lớn','Cuốn sách được chính tác giả nói là viết không dành cho trẻ con mà dành cho những ai đã từng là trẻ con. Rõ ràng, đây là cuốn sách văn học mà bạn nên tìm đọc gấp.',3,1),(5,'2020-01-02 12:00:00','2020-01-03 00:00:00',NULL,100000,0,0,'Sách Đời bạn, bạn không sống, ai sống hộ','Từng câu chuyện của tác giả đưa ra đều truyền tải rất nhiều thông điệp ý nghĩa về cách sống của người Nhật từ trước đến nay',2,1),(6,'2020-01-01 12:00:00','2020-01-02 00:00:00',NULL,10000000,0,0,'Iphone X','Apple Iphone X Black LL/A',4,2),(7,'2020-01-01 12:00:00','2020-01-02 00:00:00',NULL,15000000,0,0,'Iphone Xs','Apple Iphone Xs White LL/A',1,2),(8,'2020-01-02 12:00:00','2020-01-03 00:00:00',NULL,18000000,0,0,'Iphone 11','Apple Iphone 11 Green LL/A',5,2),(9,'2020-01-02 12:00:00','2020-01-03 00:00:00',NULL,23000000,0,0,'Iphone 11 Pro','Apple Iphone 11 Pro Black LL/A',2,2),(10,'2020-01-02 12:00:00','2020-01-03 00:00:00',NULL,25000000,0,0,'Iphone 11 ProMax','Apple Iphone 11 ProMax Black LL/A',3,2),(11,'2020-01-01 12:00:00','2020-01-02 00:00:00',NULL,25000000,0,0,'Laptop Macbook','Macbook Air 13 128GB 2019 Gold (MVFM2)',2,5),(12,'2020-01-01 12:00:00','2020-01-02 00:00:00',NULL,20000000,0,0,'Laptop Asus','Laptop Asus Zenbook 14 UX433FA-A6113T	(i5-8265U / 8GB / 256GB / 14 inch / Win 10)',5,5),(13,'2020-01-01 12:00:00','2020-01-02 00:00:00',NULL,10000000,0,0,'Laptop Dell','Laptop Dell Inspiron 14 N3480 - 8145	(i3-8145U / 4GB / 1TB / 14inch / Win10)',1,5),(14,'2020-01-02 12:00:00','2020-01-03 00:00:00',NULL,10000000,0,0,'Laptop HP','Laptop HP Pavilion 14-ce2041TU 6ZT94PA	(i5-8265U / 4GB / 1TB / 14 inch / Win 10 / Vàng)',2,5),(15,'2020-01-02 12:00:00','2020-01-03 00:00:00',NULL,15000000,0,0,'Laptop Asus','Asus Vivobook 14 A412FA-EK224T (i5-8265U / 8GB / 512GB / 14 inches / Win 10 / Bạc)',3,5),(16,'2020-01-01 12:00:00','2020-01-02 00:00:00',NULL,30000000,0,0,'Xe máy Honda Vision','Xe máy Honda Vision 2019 Trắng Nâu Đen bản tiêu chuẩn',5,5),(17,'2020-01-01 12:00:00','2020-01-02 00:00:00',NULL,40000000,0,0,'Xe máy Yamaha Grande Hybrid','Xe Máy Yamaha Grande Hybrid bản tiêu chuẩn',4,5),(18,'2020-01-01 12:00:00','2020-01-02 00:00:00',NULL,35000000,0,0,'Xe máy Yamaha Latte','Xe máy Yamaha Latte',3,5),(19,'2020-01-02 12:00:00','2020-01-03 00:00:00',NULL,40000000,0,0,'Xe máy Honda Vario','Xe máy Honda Vario 125 - Đen Nhám 2019',1,5),(20,'2020-01-02 12:00:00','2020-01-03 00:00:00',NULL,40000000,0,0,'Xe máy Honda Airblack','Honda Air Blade 125 màu Đen Bạc - phiên bản CAO CẤP',2,5);
/*!40000 ALTER TABLE `bid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Sách'),(2,'Điện thoại'),(3,'Máy chụp hình'),(4,'Quần áo-Giày dép'),(5,'Máy tính'),(6,'Đồ trang sức'),(7,'Phụ kiện điện tử'),(8,'Đồ em bé'),(9,'Điện gia dụng'),(10,'Xe máy'),(11,'Phụ kiện thời trang');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `comment` (
  `content` varchar(500) NOT NULL,
  `like` tinyint(4) NOT NULL,
  `account_id` int(11) NOT NULL,
  `bid_id` int(11) NOT NULL,
  KEY `id_idx1` (`bid_id`),
  KEY `FK_COMMENT_ACCOUNT_idx` (`account_id`),
  CONSTRAINT `FK_COMMENT_ACCOUNT` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_COMMENT_BID` FOREIGN KEY (`bid_id`) REFERENCES `bid` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-02  3:09:16
