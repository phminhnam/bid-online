var createError = require("http-errors");
var express = require("express");

var path = require("path");
var cookieParser = require("cookie-parser");
// // var logger = require("morgan");

// ///////////login
const bodyParser = require("body-parser");
const Passport = require("passport");

////////////

var indexRouter = require("./routes/index");
var categoriesRouter = require("./routes/categories");

var loginRouter = require("./routes/login");
var registerRouter = require("./routes/register");
var changepassRouter = require("./routes/changepass");
var profile = require("./routes/profile");
var editProfileRouter = require("./routes/editProfile");
var logoutRouter = require("./routes/logout");

var bidRouter = require("./routes/bids");
var app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

// ///////////
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(
  require("express-session")({
    secret: "keyboard cat",
    session: 1000 * 60 * 5,
    resave: true,
    saveUninitialized: true
  })
);
app.use(Passport.initialize());
app.use(Passport.session());
app.use((req, res, next) => {
  res.locals.user = req.user;
  next();
});

// app.use(logger("dev"));
app.use(bodyParser.json({ type: "application/*+json" }));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/categories", categoriesRouter);
app.use("/login", loginRouter);
app.use("/register", registerRouter);
app.use("/changepass", changepassRouter);
app.use("/profile", profile);
app.use("/editProfile", editProfileRouter);
app.use("/logout", logoutRouter);
app.use("/bid", bidRouter);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error", { message: err.message });
});

module.exports = app;
